from django.conf.urls import url
from polls import views
from django.contrib.auth import views as auth_views
from . import views as core_views

urlpatterns = [
    url(r'^maps/$', auth_views.login, {'template_name': 'maps/maps.html'}, name='maps'),
]